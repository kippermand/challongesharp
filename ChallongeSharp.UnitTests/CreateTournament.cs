﻿using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using System.Threading.Tasks;

namespace ChallongeSharp.UnitTests
{
    public class CreateTournament
    {
        [Test]
        public async Task Test()
        {
            var result = await Execute();

            var result2 = await DeleteTournament.Execute(result.Id.Value);

            Assert.That(result2, Is.True);
        }

        internal async static Task<Models.ChallongeModels.Tournament> Execute()
        {
            var config = Initializers.BuildConfig();

            var tournamentClient = config.GetTournamentClient();

            var tournament = new Models.ViewModels.TournamentVm
            {
                Name = "test",
                Type = Models.ViewModels.Types.TournamentType.SingleElimination,
                ShowRounds = true,
                Url = "testingNEBAShortURL"
            };

            return await tournamentClient.CreateTournamentAsync(tournament);
        }
    }
}
