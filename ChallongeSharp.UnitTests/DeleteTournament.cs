﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChallongeSharp.UnitTests
{
    public class DeleteTournament
    {
        internal async static Task<bool> Execute(long tournamentId)
        {
            var config = Initializers.BuildConfig();

            var tournamentClient = config.GetTournamentClient();

            return await tournamentClient.DeleteTournamentAsync(tournamentId);
        }
    }
}
