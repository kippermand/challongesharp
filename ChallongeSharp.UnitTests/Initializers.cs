﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChallongeSharp.UnitTests
{
    internal static class Initializers
    {
        internal static Clients.ITournamentClient GetTournamentClient(this IConfiguration config)
        {

            var challongeClient = config.GetChallongeClient();
            

            return new Clients.TournamentClient(challongeClient);
        }

        internal static Clients.IChallongeClient GetChallongeClient(this IConfiguration config)
        {
            var httpClient = new System.Net.Http.HttpClient();

            var challongeConfig = new Models.Configurations.ChallongeConfigurations
            {
                Username = config.GetValue<string>("username"),
                ApiKey = config.GetValue<string>("apiKey")
            };

           return new Clients.ChallongeClient(httpClient, Microsoft.Extensions.Options.Options.Create(challongeConfig));
        }

        internal static IConfiguration BuildConfig()
            => new ConfigurationBuilder()
                           .SetBasePath(System.AppDomain.CurrentDomain.BaseDirectory)
                           .AddUserSecrets<CreateTournament>()
                           .Build();
    }
}
