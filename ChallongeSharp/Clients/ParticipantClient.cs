using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChallongeSharp.Helpers;
using ChallongeSharp.Models.ChallongeModels;
using ChallongeSharp.Models.ViewModels;

namespace ChallongeSharp.Clients
{
    public class ParticipantClient : IParticipantClient
    {
        private readonly IChallongeClient _client;

        public ParticipantClient(IChallongeClient client)
        {
            _client = client;
        }

        #region GetRequests

        public async Task<List<Participant>> GetAllParticipantsAsync(long tournamentId)
        {
            var requestUrl = $"tournaments/{tournamentId}/participants.json";
            
            var participants = await _client.GetAsync<List<ParticipantResponse>>(requestUrl);
            return participants.Select(p => p.Participant).ToList();
        }

        public async Task<Participant> GetParticipantAsync(long tournamentId, string participantId)
        {
            var requestUrl = $"tournaments/{tournamentId}/participants/{participantId}.json";

            var participant = await _client.GetAsync<ParticipantResponse>(requestUrl);
            return participant.Participant;
        }

        #endregion

        #region PostRequests

        public async Task<Participant> AddParticipantAsync(ParticipantVm participantVm, long tournamentId)
        {
            var requestUrl = $"tournaments/{tournamentId}/participants.json";
            var requestContent = participantVm.ToRequestContent();

            var participant = await _client.PostAsync<ParticipantResponse>(requestUrl, requestContent);
            return participant.Participant;
        }

        public async Task<List<Participant>> BulkAddParticipantsAsync(List<ParticipantVm> participantVms,
            long tournamentId)
        {
            var participants = new List<Participant>();
            foreach (var participant in participantVms)
            {
                participants.Add(await AddParticipantAsync(participant, tournamentId));
            }

            return participants;
        }

        public async Task<Participant> CheckInParticipantAsync(string participantId, long tournamentId)
        {
            var requestUrl = $"tournaments/{tournamentId}/participants/{participantId}/check_in.json";

            var participant = await _client.PostAsync<ParticipantResponse>(requestUrl);
            return participant.Participant;
        }

        public async Task<Participant> UndoCheckInParticipantAsync(string participantId, long tournamentId)
        {
            var requestUrl = $"tournaments/{tournamentId}/participants/{participantId}/undo_check_in.json";

            var participant = await _client.PostAsync<ParticipantResponse>(requestUrl);
            return participant.Participant;
        }

        public async Task<Participant> RandomizeParticipantsAsync(long tournamentId)
        {
            var requestUrl = $"tournaments/{tournamentId}/participants/randomize.json";

            var participant = await _client.PostAsync<ParticipantResponse>(requestUrl);
            return participant.Participant;
        }

        #endregion

        #region PutRequests

        public async Task<Participant> UpdateParticipantAsync(ParticipantVm participantVm, long tournamentId)
        {
            var requestUrl = $"tournaments/{tournamentId}/particpants/{participantVm.Id}.json";

            var participant = await _client.PutAsync<ParticipantResponse>(requestUrl, participantVm);
            return participant.Participant;
        }

        #endregion

        #region DeleteRequests

        public async Task<bool> RemoveParticipantAsync(string participantId, long tournamentId)
        {
            var requestUrl = $"tournaments/{tournamentId}/participants/{participantId}.json";

            var success = await _client.DeleteAsync(requestUrl);
            return success;
        }

        public async Task<bool> ClearAllParticipants(long tournamentId)
        {
            var requestUrl = $"tournaments/{tournamentId}/participants/clear.json";

            var success = await _client.DeleteAsync(requestUrl);
            return success;
        }

        #endregion
    }

    public interface IParticipantClient
    {
        Task<List<Participant>> GetAllParticipantsAsync(long tournamentId);
        Task<Participant> GetParticipantAsync(long tournamentId, string participantId);
        Task<Participant> AddParticipantAsync(ParticipantVm participantVm, long tournamentId);

        Task<List<Participant>> BulkAddParticipantsAsync(List<ParticipantVm> participantVms,
            long tournamentId);

        Task<Participant> CheckInParticipantAsync(string participantId, long tournamentId);
        Task<Participant> UndoCheckInParticipantAsync(string participantId, long tournamentId);
        Task<Participant> RandomizeParticipantsAsync(long tournamentId);
        Task<Participant> UpdateParticipantAsync(ParticipantVm participantVm, long tournamentId);
        Task<bool> RemoveParticipantAsync(string participantId, long tournamentId);
        Task<bool> ClearAllParticipants(long tournamentId);
    }
}