using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChallongeSharp.Models.ChallongeModels;
using ChallongeSharp.Models.ViewModels;
using ChallongeSharp.Models.ViewModels.Types;

namespace ChallongeSharp.Clients
{
    public class TournamentClient : ITournamentClient
    {
        private readonly IChallongeClient _client;

        public TournamentClient(IChallongeClient client)
        {
            _client = client;
        }

        #region GetRequests

        public async Task<List<Tournament>> GetAllTournamentsAsync(TournamentState state = null,
            TournamentType type = null, DateTime? createdAfter = null, DateTime? createdBefore = null,
            string subdomain = null)
        {
            var options = new TournamentOptions
            {
                State = state,
                Type = type,
                CreatedAfter = createdAfter,
                CreatedBefore = createdBefore,
                Subdomain = subdomain
            };

            var tournaments = await _client.GetAsync<List<TournamentResponse>>("tournaments.json", options);
            return tournaments.Select(t => t.Tournament).ToList();
        }

        public async Task<List<Tournament>> GetTournamentAsync(string name, bool includeParticipants = false,
            bool includeMatches = false)
        {
            var options = new TournamentOptions
            {
                IncludeParticipants = includeParticipants,
                IncludeMatches = includeMatches
            };

            var tournaments = await _client.GetAsync<List<TournamentResponse>>($"tournaments/{name}.json", options);
            return tournaments.Select(t => t.Tournament).ToList();
        }

        #endregion

        #region PostRequests

        public async Task<Tournament> CreateTournamentAsync(TournamentVm tournamentVm)
        {
            var tournament = await _client.PostAsync<TournamentResponse>("tournaments.json", tournamentVm);
            return tournament.Tournament;
        }

        public async Task<Tournament> ProcessCheckInsAsync(long tournamentId, bool includeParticipants = false,
            bool includeMatches = false)
        {
            string requestUrl = $"tournaments/{tournamentId}/process_check_ins.json";
            var options = new TournamentOptions
                {IncludeParticipants = includeParticipants, IncludeMatches = includeMatches};

            TournamentResponse tournament = await _client.PostAsync<TournamentResponse>(requestUrl, options);
            return tournament.Tournament;
        }

        public async Task<Tournament> AbortCheckInsAsync(long tournamentId, bool includeParticipants = false,
            bool includeMatches = false)
        {
            string requestUrl = $"tournaments/{tournamentId}/abort_check_in.json";
            var options = new TournamentOptions
                {IncludeParticipants = includeParticipants, IncludeMatches = includeMatches};

            TournamentResponse tournament = await _client.PostAsync<TournamentResponse>(requestUrl, options);
            return tournament.Tournament;
        }

        public async Task<Tournament> StartAsync(long tournamentId, bool includeParticipants = false,
            bool includeMatches = false)
        {
            string requestUrl = $"tournaments/{tournamentId}/start.json";
            var options = new TournamentOptions
                {IncludeParticipants = includeParticipants, IncludeMatches = includeMatches};

            TournamentResponse tournament = await _client.PostAsync<TournamentResponse>(requestUrl, options);
            return tournament.Tournament;
        }

        public async Task<Tournament> FinalizeAsync(long tournamentId, bool includeParticipants = false,
            bool includeMatches = false)
        {
            string requestUrl = $"tournaments/{tournamentId}/finalize.json";
            var options = new TournamentOptions
                {IncludeParticipants = includeParticipants, IncludeMatches = includeMatches};

            TournamentResponse tournament = await _client.PostAsync<TournamentResponse>(requestUrl, options);
            return tournament.Tournament;
        }

        public async Task<Tournament> ResetAsync(long tournamentId, bool includeParticipants = false,
            bool includeMatches = false)
        {
            string requestUrl = $"tournaments/{tournamentId}/finalize.json";
            var options = new TournamentOptions
                {IncludeParticipants = includeParticipants, IncludeMatches = includeMatches};

            TournamentResponse tournament = await _client.PostAsync<TournamentResponse>(requestUrl, options);
            return tournament.Tournament;
        }

        public async Task<Tournament> OpenForPredictionsAsync(long tournamentId, bool includeParticipants = false,
            bool includeMatches = false)
        {
            string requestUrl = $"tournaments/{tournamentId}/open_for_predictions.json";
            var options = new TournamentOptions
                {IncludeParticipants = includeParticipants, IncludeMatches = includeMatches};

            TournamentResponse tournament = await _client.PostAsync<TournamentResponse>(requestUrl, options);
            return tournament.Tournament;
        }

        #endregion

        #region PutRequests

        public async Task<Tournament> UpdateTournamentAsync(TournamentVm tournamentVm)
        {
            var requestUrl = $"tournaments/{tournamentVm.Name}.json";
            
            var tournament = await _client.PutAsync<TournamentResponse>(requestUrl, tournamentVm);
            return tournament.Tournament;
        }

        #endregion

        #region DeleteRequests

        public async Task<bool> DeleteTournamentAsync(long tournamentId)
        {
            return await _client.DeleteAsync($"tournaments/{tournamentId}.json");
        }

        #endregion
    }

    public interface ITournamentClient
    {
        Task<List<Tournament>> GetAllTournamentsAsync(TournamentState state = null,
            TournamentType type = null, DateTime? createdAfter = null, DateTime? createdBefore = null,
            string subdomain = null);

        Task<List<Tournament>> GetTournamentAsync(string name, bool includeParticipants = false,
            bool includeMatches = false);

        Task<Tournament> CreateTournamentAsync(TournamentVm tournamentVm);

        Task<Tournament> ProcessCheckInsAsync(long tournamentId, bool includeParticipants = false,
            bool includeMatches = false);

        Task<Tournament> AbortCheckInsAsync(long tournamentId, bool includeParticipants = false,
            bool includeMatches = false);

        Task<Tournament> StartAsync(long tournamentId, bool includeParticipants = false,
            bool includeMatches = false);

        Task<Tournament> FinalizeAsync(long tournamentId, bool includeParticipants = false,
            bool includeMatches = false);

        Task<Tournament> ResetAsync(long tournamentId, bool includeParticipants = false,
            bool includeMatches = false);

        Task<Tournament> OpenForPredictionsAsync(long tournamentId, bool includeParticipants = false,
            bool includeMatches = false);

        Task<Tournament> UpdateTournamentAsync(TournamentVm tournamentVm);
        Task<bool> DeleteTournamentAsync(long tournamentId);
    }
}