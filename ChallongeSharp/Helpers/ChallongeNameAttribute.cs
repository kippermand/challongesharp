using System;

namespace ChallongeSharp.Helpers
{
    [AttributeUsage(AttributeTargets.Property)]
    public class ChallongeNameAttribute : Attribute
    {
        public ChallongeNameAttribute(string name)
        {
            AttributeName = name;
        }

        public string Name => AttributeName;

        private string AttributeName { get; }
    }
}