using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using ChallongeSharp.Models.ViewModels.BaseModels;

namespace ChallongeSharp.Helpers
{
    public static class HttpHelper
    {
        public static FormUrlEncodedContent ToRequestContent(this BaseModel model)
        {
            var properties = model.GetType().GetProperties();
            var requestContent = properties
                .Where(prop => prop.GetValue(model) != null && Attribute.IsDefined(prop, typeof(ChallongeNameAttribute)))
                .Select(prop => GetKvpRequestParam(prop, model)).ToList();

            return new FormUrlEncodedContent(requestContent);
        }

        public static string ToRequestParams(this BaseModel options)
        {
            var properties = options.GetType().GetProperties();
            var requestParams = properties
                .Where(prop =>
                    prop.GetValue(options) != null && Attribute.IsDefined(prop, typeof(ChallongeNameAttribute)))
                .Select(prop => GetStringRequestParam(prop, options)).ToList();

            return $"{string.Join("&", requestParams)}";
        }

        private static KeyValuePair<string, string> GetKvpRequestParam<T>(PropertyInfo property, T viewModel)
        {
            object propertyValue = property.GetValue(viewModel);
            AttributeCollection attributes = TypeDescriptor.GetProperties(viewModel)[property.Name].Attributes;
            string propertyDescription = ((ChallongeNameAttribute) attributes[typeof(ChallongeNameAttribute)]).Name;
            return new KeyValuePair<string, string>(propertyDescription, propertyValue.ToString());
        }

        private static string GetStringRequestParam<T>(PropertyInfo property, T viewModel)
        {
            object propertyValue = property.GetValue(viewModel);
            AttributeCollection attributes = TypeDescriptor.GetProperties(viewModel)[property.Name].Attributes;
            string propertyDescription = ((ChallongeNameAttribute) attributes[typeof(ChallongeNameAttribute)]).Name;
            if (propertyValue.GetType().Name.Equals(typeof(DateTime).Name))
                propertyValue = ((DateTime) propertyValue).ToString("yyyy-MM-dd");

            return $"{propertyDescription}={propertyValue}";
        }
    }
}