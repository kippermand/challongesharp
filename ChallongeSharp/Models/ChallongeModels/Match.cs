using System;
using System.Text.Json.Serialization;

namespace ChallongeSharp.Models.ChallongeModels
{
    public class MatchResponse
    {
        public Match Match { get; set; }
    }
    
    public class Match
    {
        [JsonPropertyName("attachment_count")] public int? AttachmentCount { get; set; }

        [JsonPropertyName("created_at")] public DateTime? CreatedAt { get; set; }

        [JsonPropertyName("group_id")] public long? GroupId { get; set; }

        [JsonPropertyName("has_attachment")] public bool? HasAttachment { get; set; }

        [JsonPropertyName("id")] public long? Id { get; set; }

        [JsonPropertyName("identifier")] public string Identifier { get; set; }

        [JsonPropertyName("location")] public string Location { get; set; }

        [JsonPropertyName("loser_id")] public long? LoserId { get; set; }

        [JsonPropertyName("player1_id")] public long? Player1Id { get; set; }

        [JsonPropertyName("player1_is_prereq_match_loser")]
        public bool? Player1IsPrereqMatchLoser { get; set; }

        [JsonPropertyName("player1_prereq_match_id")]
        public long? Player1PrereqMatchId { get; set; }

        [JsonPropertyName("player1_votes")] public int? Player1Votes { get; set; }

        [JsonPropertyName("player2_id")] public long? Player2Id { get; set; }

        [JsonPropertyName("player2_is_prereq_match_loser")]
        public bool? Player2IsPrereqMatchLoser { get; set; }

        [JsonPropertyName("player2_prereq_match_id")]
        public long? Player2PrereqMatchId { get; set; }

        [JsonPropertyName("player2_votes")] public int? Player2Votes { get; set; }

        [JsonPropertyName("round")] public long? Round { get; set; }

        [JsonPropertyName("scheduled_time")] public DateTime? ScheduledTime { get; set; }

        [JsonPropertyName("started_at")] public DateTime? StartedAt { get; set; }

        [JsonPropertyName("state")] public string State { get; set; }

        [JsonPropertyName("tournament_id")] public long? TournamentId { get; set; }

        [JsonPropertyName("underway_at")] public DateTime? UnderwayAt { get; set; }

        [JsonPropertyName("updated_at")] public DateTime? UpdatedAt { get; set; }

        [JsonPropertyName("winner_id")] public long? WinnerId { get; set; }

        [JsonPropertyName("prerequisite_match_ids_csv")]
        public string PrerequisiteMatchIdsCsv { get; set; }

        [JsonPropertyName("scores_csv")] public string ScoresCsv { get; set; }
    }
}