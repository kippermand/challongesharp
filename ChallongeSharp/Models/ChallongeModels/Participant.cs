using System;
using System.Text.Json.Serialization;

namespace ChallongeSharp.Models.ChallongeModels
{
    public class ParticipantResponse
    {
        public Participant Participant { get; set; }
    }

    public class Participant
    {
        [JsonPropertyName("active")] public bool Active { get; set; }

        [JsonPropertyName("checked_in_at")] public DateTime? CheckedInAt { get; set; }

        [JsonPropertyName("created_at")] public DateTime? CreatedAt { get; set; }

        [JsonPropertyName("final_rank")] public int? FinalRank { get; set; }

        [JsonPropertyName("group_id")] public long? GroupId { get; set; }

        [JsonPropertyName("icon")] public string Icon { get; set; }

        [JsonPropertyName("id")] public long? Id { get; set; }

        [JsonPropertyName("invitation_id")] public long? InvitationId { get; set; }

        [JsonPropertyName("invite_email")] public string InviteEmail { get; set; }

        [JsonPropertyName("misc")] public string Misc { get; set; }

        [JsonPropertyName("name")] public string Name { get; set; }

        [JsonPropertyName("on_waiting_list")] public bool? OnWaitingList { get; set; }

        [JsonPropertyName("seed")] public long? Seed { get; set; }

        [JsonPropertyName("tournament_id")] public long? TournamentId { get; set; }

        [JsonPropertyName("updated_at")] public DateTime? UpdatedAt { get; set; }

        [JsonPropertyName("challonge_username")] public string ChallongeUsername { get; set; }

        [JsonPropertyName("challonge_email_address_verified")]
        public bool? ChallongeEmailAddressVerified { get; set; }

        [JsonPropertyName("removable")] public bool? Removable { get; set; }

        [JsonPropertyName("participatable_or_invitation_attached")]
        public bool? ParticipatableOrInvitationAttached { get; set; }

        [JsonPropertyName("confirm_remove")] public bool? ConfirmRemove { get; set; }

        [JsonPropertyName("invitation_pending")] public bool? InvitationPending { get; set; }

        [JsonPropertyName("display_name_with_invitation_email_address")]
        public string DisplayNameWithInvitationEmailAddress { get; set; }

        [JsonPropertyName("email_hash")] public string EmailHash { get; set; }

        [JsonPropertyName("username")] public string Username { get; set; }

        [JsonPropertyName("attached_participatable_portrait_url")]
        public string AttachedParticipatablePortraitUrl { get; set; }

        [JsonPropertyName("can_check_in")] public bool? CanCheckIn { get; set; }

        [JsonPropertyName("checked_in")] public bool? CheckedIn { get; set; }

        [JsonPropertyName("reactivatable")] public bool? Reactivatable { get; set; }
    }
}