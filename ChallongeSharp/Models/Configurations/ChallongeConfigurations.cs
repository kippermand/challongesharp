using System;
using System.Text;

namespace ChallongeSharp.Models.Configurations
{
    public class ChallongeConfigurations
    {
        public string Username { get; set; }
        public string ApiKey { get; set; }

        internal string Token =>
            Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(Username + ":" + ApiKey));
    }
}