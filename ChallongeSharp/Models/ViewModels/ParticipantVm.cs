using ChallongeSharp.Helpers;
using ChallongeSharp.Models.ViewModels.BaseModels;

namespace ChallongeSharp.Models.ViewModels
{
    public class ParticipantVm : BaseModel
    {
        [ChallongeName("participant[name]")]
        public string ParticipantName { get; set; }
        [ChallongeName("participant[challonge_username]")]
        public string ChallongeUsername { get; set; }
        [ChallongeName("participant[email]")]
        public string Email { get; set; }
        [ChallongeName("participant[seed]")]
        public int Seed { get; set; } 
        [ChallongeName("participant[misc]")]
        public string Misc { get; set; }
        public long Id { get; set; }
    }
}