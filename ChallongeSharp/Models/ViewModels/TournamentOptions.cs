using System;
using ChallongeSharp.Helpers;
using ChallongeSharp.Models.ViewModels.BaseModels;
using ChallongeSharp.Models.ViewModels.Types;

namespace ChallongeSharp.Models.ViewModels
{
    public class TournamentOptions : BaseModel
    {
        public string Name { get; set; }
        [ChallongeName("state")] public TournamentState State { get; set; }
        [ChallongeName("type")] public TournamentType Type { get; set; }
        [ChallongeName("created_after")] public DateTime? CreatedAfter { get; set; }
        [ChallongeName("created_before")] public DateTime? CreatedBefore { get; set; }
        [ChallongeName("subdomain")] public string Subdomain { get; set; }
        public bool IncludeParticipants { get; set; }
        public bool IncludeMatches { get; set; }

        [ChallongeName("include_participants")]
        public int IncludeParticipantsInt => Convert.ToInt32(IncludeParticipants);

        [ChallongeName("include_matches")] public int IncludeMatchesInt => Convert.ToInt32(IncludeMatches);
    }
}