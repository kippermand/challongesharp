using System;
using ChallongeSharp.Helpers;
using ChallongeSharp.Models.ViewModels.BaseModels;
using ChallongeSharp.Models.ViewModels.Types;

namespace ChallongeSharp.Models.ViewModels
{
    public class TournamentVm : BaseModel
    {
        [ChallongeName("tournament[name]")] public string Name { get; set; }

        [ChallongeName("tournament[tournament_type]")]
        public TournamentType Type { get; set; }

        [ChallongeName("tournament[url]")] public string Url { get; set; }

        [ChallongeName("tournament[subdomain]")]
        public string Subdomain { get; set; }

        [ChallongeName("tournament[description]")]
        public string Description { get; set; }

        [ChallongeName("tournament[open_signup]")]
        public bool? OpenSignup { get; set; }

        [ChallongeName("tournament[hold_third_place_match]")]
        public bool? HoldThirdPlaceMatch { get; set; }

        [ChallongeName("tournament[pts_for_match_win]")]
        public decimal? PointsForMatchWin { get; set; }

        [ChallongeName("tournament[pts_for_match_tie]")]
        public decimal? PointsForMatchTie { get; set; }

        [ChallongeName("tournament[pts_for_game_win]")]
        public decimal? PointsForGameWin { get; set; }

        [ChallongeName("tournament[pts_for_game_tie]")]
        public decimal? PointsForGameTie { get; set; }

        [ChallongeName("tournament[pts_for_bye]")]
        public decimal? PointsForBye { get; set; }

        [ChallongeName("ournament[swiss_rounds]")]
        public int? SwissRounds { get; set; }

        [ChallongeName("tournament[ranked_by]")]
        public RankedBy RankedBy { get; set; }

        [ChallongeName("tournament[rr_pts_for_match_win]")]
        public decimal? RoundRobinPoinsForMatchWin { get; set; }

        [ChallongeName("tournament[rr_pts_for_match_tie]")]
        public decimal? RoundRobinPoinsForMatchTie { get; set; }

        [ChallongeName("tournament[rr_pts_for_game_win]")]
        public decimal? RoundRobinPoinsForGameWin { get; set; }

        [ChallongeName("tournament[rr_pts_for_game_tie]")]
        public decimal? RoundRobinPoinsForGameTie { get; set; }

        [ChallongeName("tournament[accept_attachments]")]
        public bool? AcceptAttachments { get; set; }

        [ChallongeName("tournament[hide_forum]")]
        public bool? HideForm { get; set; }

        [ChallongeName("tournament[show_rounds]")]
        public bool? ShowRounds { get; set; }

        [ChallongeName("tournament[private]")] public bool? Private { get; set; }

        [ChallongeName("tournament[notify_users_when_matches_open]")]
        public bool? NotifyUsersWhenMatchesOpen { get; set; }

        [ChallongeName("tournament[notify_users_when_the_tournament_ends]")]
        public bool? NotifyUsersWhenTheTournamentEnds { get; set; }

        [ChallongeName("tournament[sequential_pairings]")]
        public bool? SequentialPairings { get; set; }

        [ChallongeName("tournament[signup_cap]")]
        public int? SignupCap { get; set; }

        [ChallongeName("tournament[start_at]")]
        public DateTime? StartAt { get; set; }

        [ChallongeName("tournament[check_in_duration]")]
        public int? CheckInDuration { get; set; }

        [ChallongeName("tournament[grand_finals_modifier]")]
        public GrandFinalsModifier GrandFinalsModifier { get; set; }
    }
}