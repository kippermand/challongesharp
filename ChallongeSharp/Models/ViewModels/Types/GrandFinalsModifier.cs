namespace ChallongeSharp.Models.ViewModels.Types
{
    public class GrandFinalsModifier
    {
        public GrandFinalsModifier(string value)
        {
            Value = value;
        }

        public string Value { get; set; }

        public static GrandFinalsModifier Default => null;
        public static GrandFinalsModifier SingleMatch => new GrandFinalsModifier("single match");
        public static GrandFinalsModifier Skip => new GrandFinalsModifier("skip");

        public override string ToString()
        {
            return Value;
        }
    }
}