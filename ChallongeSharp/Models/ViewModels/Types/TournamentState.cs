namespace ChallongeSharp.Models.ViewModels.Types
{
    public class TournamentState
    {
        public TournamentState(string value)
        {
            Value = value;
        }

        public string Value { get; set; }

        public static TournamentState All => new TournamentState("all");
        public static TournamentState Pending => new TournamentState("pending");
        public static TournamentState InProgress => new TournamentState("in_progress");
        public static TournamentState Ended => new TournamentState("ended");

        public override string ToString()
        {
            return Value;
        }
    }
}