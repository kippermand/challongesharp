This is an open sourced client for the Challonge API designed for .NET
It is written in .NET Standard 2.0, you can find the frameworks compatible with
it here
https://docs.microsoft.com/en-us/dotnet/standard/net-standard